#!/bin/bash

while getopts ":B:l:n:" o; do
    case "${o}" in
	l)
	    BUILDS_LOG=${OPTARG}
	    ;;
	n)
	    NUM_BUILDS=${OPTARG}
	    ;;
	B)
	    BUILD=${OPTARG}
	    ;;
        *)
	    echo "Unrecognised arg ${o}"
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

export BUILD

# Pull in the core settings for all builds
. settings.sh
. weekly.sh
. common.sh

START=$(now)

# Pull in common config based on our filename. Look for specific
# settings, based on the components of the build name. We also look
# for specific config based on the full name LAST, to allow for
# over-riding the config we've built up.
#echo "Looking for config for $BUILD"
if [ "$CONFDUMP"x != ""x ]; then
    echo "#### $BUILD config start: ####"
fi
for CFG in $(echo $BUILD | sed 's/^B_//g;s/_/ /g') $BUILD; do
    if [ -f $CFG.cfg ]; then
	CONFIG_USED="$CONFIG_USED $CFG.cfg"
#	echo "$BUILD: Including configuration from $CFG.cfg"
	if [ "$CONFDUMP"x != ""x ]; then
	    echo "#### $CFG.cfg start ####"
	    cat $CFG.cfg
	    echo "#### $CFG.cfg end ####"
	fi
	. $CFG.cfg
    else
	CONFIG_UNUSED="$CONFIG_UNUSED $CFG.cfg"
#	echo "$BUILD: No config available in $CFG.cfg"
    fi
done

export CONFIG_USED
export CONFIG_UNUSED

if [ "$CONFDUMP"x != ""x ]; then
    echo "#### $BUILD config end: ####"
    exit 0
fi

if [ "$DRYRUN"x != ""x ]; then
    echo "Dry-run $0: BUILD $BUILD"
    if [ $DRYRUN != count ]; then
	sleep $DRYRUN_WAIT
    fi
    exit 0
fi

# Sanity check - make sure we have certain variables set
check_variables ARCH CODENAME DI_CODENAME DI_DIST \
		MAXISOS MAXJIGDOS INSTALLER_CD

lockfile -! -l 43200 -r-1 $BUILDS_LOG.lock
echo $BUILD >> $BUILDS_LOG
BUILDNUM=$(wc -l < $BUILDS_LOG)
rm -f $BUILDS_LOG.lock
SHOW_BUILDS="$BUILDNUM/$NUM_BUILDS: "
export SHOW_BUILDS

# Actually execute the build step
./testingcds "$ARCH"

ret=$?

END=$(now)
SPENT=$(calc_time $START $END)
if [ $ret = 0 ]; then
    echo "  $BUILD finished successfully (started at $START, ended at $END, took $SPENT)"
else
    echo "  $BUILD failed (started at $START, ended at $END, took $SPENT)"
fi

# We do NOT exit non-zero here, we want to build all we can. Instead,
# weekly-build-finalise-arch.sh will pick up on the errors later.
exit 0

