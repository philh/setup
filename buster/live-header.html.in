#if 0
Input file for docs in multiple different directories. Process with
cpp.
#endif

<h1>What's in this directory?</h1>

#ifdef ARCHsource

<p>These are files containing source code for the Debian live
images.</p>

#else

<p>These are files containing live images for the Debian GNU/KERNEL
operating system. They are specifically for the <code>ARCH</code>
architecture.</p>

#endif

<p>There are multiple different files here, corresponding to the
different desktop environments..</p>

<h2>How do I use these files?</h2>

#if defined(STYLEbt)

<p>The files here are bittorrent files. Use
a <a href="https://www.debian.org/CD/torrent-cd/">bittorrent</a>
client program to download the contents of the complete ISO image
files in a peer-to-peer manner. You should end up with an exact copy
of each ISO image as though you'd downloaded it directly via HTTP or
FTP.</p>

#elif defined(STYLEiso)

<p>The files here are complete ISO images, ready to use.</p>

#endif

<p> Once you have downloaded all the ISO images you want, you will
typically need to write them to media, either writeable DVD or a USB
stick.</p>

<h2>How can I verify my download is correct and exactly what has
   been created by Debian?</h2>

<p>There are files here (SHA512SUMS, etc.) which contain checksums
of the images. These checksum files are also signed - see the
matching .sign files. Once you've downloaded an image, you can
check:</p>

<ul>
   <li>that its checksum matches that expected from the checksum file; and
   <li>that the checksum file has not been tampered with.
</ul>

<p>For more information about how to do these steps, read
the <a href="https://www.debian.org/CD/verify">verification guide</a>.

#ifdef FIRMWARE

  <h2>Non-free Firmware</h2>

  <p>For convenience for some users, this <strong>unofficial</strong>
  alternative build
  includes <a href="https://wiki.debian.org/Firmware">non-free
  firmware</a> for extra support for some awkward hardware.</p>

#endif

#ifdef ARCH_HAS_FIRMWARE

  <h2>Non-free Firmware</h2>

  <p>This is an <strong>official</strong> Debian image build and so
  only includes Free Software.</p>

  <p>For convenience for some users, there is an
  alternative <strong>unofficial</strong> image build which
  includes <a href="https://wiki.debian.org/Firmware">non-free
  firmware</a> for extra support for some awkward hardware. Look under
  <a href="/images/unofficial/non-free/images-including-firmware/">/images/unofficial/non-free/images-including-firmware/</a>
  if you need that image instead.</p>

#endif

#ifndef ARCHsource
<h2>Memory usage</h2>

<p>Live images tend to be resource hungry by nature - they need to use
  memory to extract and store the compressed system <strong>as well
  as</strong> the memory that the running software would normally
  need. The minimum recommended RAM for using a desktop environment on
  a live image is 2 GiB. If you have a system with less memory, your
  system will not work well here.
</p>

#endif

<h2>Other questions?</h2>

<p>See the Debian CD <a href="https://www.debian.org/CD/faq/">FAQ</a>
for lots more information about Debian CDs and installation.</p>

<p>The images here were put together by
the <a href="https://wiki.debian.org/Teams/DebianCd">Debian CD
team</a>, using live-wrapper and other software.</p>
