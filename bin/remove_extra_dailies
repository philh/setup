#!/bin/bash
#
# remove_extra_dailies - clean up any builds that failed
# then remove all but the most recent few for each architecture
#
# Old version written by manty
# Tweaked/rewritten by Steve McIntyre

. ~/build/settings.sh

NUM_TO_KEEP=6

# Remove the oldest daily builds, leave only the last 6 for each arch
remove_oldest () {
    DIR=$1
    ARCHLIST=$2
    echo "  Removing old arch directories:"
    cd $DIR
    for ARCH in $ARCHLIST
    do
	if [ $ARCH != source ]; then
            NUM=`ls -1d 20*/"$ARCH"/ 2>/dev/null |wc -l`
	    NUM=$(($NUM-$NUM_TO_KEEP))
	    if [ "$NUM" -gt "0" ] ; then
		REMOVE=`ls -1d 20*/"$ARCH" 2>/dev/null|head -n $NUM`
		echo "    $REMOVE"
		rm -rf $REMOVE
	    fi
	fi
    done
}

# Check for any arch directories that are just empty shells with no
# actual files there
remove_empty_dirs () {
    DIR=$1
    ARCHLIST=$2
    echo "  Removing empty arch dirs"
    for ARCH in $ARCHLIST
    do
	if [ $ARCH != source ]; then
	    for BUILD in $DIR/20*/$ARCH ; do
		NUM_FILES=`find $BUILD -type f | wc -l`
		if [ $NUM_FILES -eq 0 ] ; then
		    echo "    $BUILD"
		    rm -rf $BUILD
		fi
	    done
	fi
    done
    echo "  Removing empty top-level build directories:"
    rmdir $DIR/20* 2>/dev/null
    if [ $? -eq 0 ] ; then
	echo "    $DIR"
    fi
}

# Update the top-level "current" link and set up arch-latest links for
# the d-i web pages
update_links () {
    DIR=$1
    ARCHLIST=$2
    echo "  Updating \"current\" link:"
    cd $DIR
    if [ -L current ] ; then
	rm current
    fi
    NEW=`ls -1d 20* 2>/dev/null | tail -n 1`
    if [ "$NEW"x != ""x ] ; then
	echo "    $DIR/current now $NEW"
	ln -s $NEW current
    fi

    echo "  Updating \"arch-latest\" links:"
    mkdir -p $DIR/arch-latest
    cd $DIR/arch-latest
    for ARCH in $ARCHLIST
    do
	if [ $ARCH != source ]; then
            rm -f $ARCH
            NEW=`ls -1d ../20*/$ARCH 2>/dev/null | tail -n 1`
	    if [ "$NEW"x != ""x ] ; then
		echo "    $DIR $ARCH latest now $NEW"
		ln -s $NEW $ARCH
	    fi
	fi
    done
}

echo "Cleaning up dailies in $OUT_BASE/daily-builds/sid_d-i:"
remove_oldest $OUT_BASE/daily-builds/sid_d-i "$ARCHES"
remove_empty_dirs $OUT_BASE/daily-builds/sid_d-i "$ARCHES"
update_links $OUT_BASE/daily-builds/sid_d-i "$ARCHES"
