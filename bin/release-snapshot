#!/bin/bash
#
# Publish jigdo files for a release, then update the snapshot
#
# Steve McIntyre

. ~/build/settings.sh

set -e

HOSTNAME=`hostname --fqdn`
case $HOSTNAME in
    pettersson*.debian.org)
        echo "Running on $HOSTNAME, OK"
        ;;
    *)
        echo "$0: Can only run on pettersson*"
        exit 1
        ;;
esac

SNAPSHOTS=/mnt/nfs-cdimage/.snapshot-jigdo/
VERSION="$1"

if [ "$VERSION"x = ""x ]; then
    echo "$0: Need a version number to work with, abort"
    exit 1
fi

INCLUDE_NF=1

case $VERSION in
    *bookworm*|12.*|*trixie*|13.*)
	# We don't have NF images for bookworm onwards
	INCLUDE_NF=0
	;;
esac

# Look for input files in a few places
MAIN_RELEASE="/mnt/nfs-cdimage/release/${VERSION}"
if [ ! -d "$MAIN_RELEASE" ]; then
    MAIN_RELEASE="/mnt/nfs-cdimage/archive/${VERSION}"
    if [ ! -d "$MAIN_RELEASE" ]; then
	MAIN_RELEASE="/mnt/nfs-cdimage/${VERSION}/debian-cd"
	if [ ! -d "$MAIN_RELEASE" ]; then
	    MAIN_RELEASE="/mnt/nfs-cdimage/.${VERSION}/debian-cd"
	    if [ ! -d "$MAIN_RELEASE" ]; then
		MAIN_RELEASE="/mnt/nfs-cdimage/${VERSION}"
		if [ ! -d "$MAIN_RELEASE" ]; then
		    echo "Can't find $MAIN_RELEASE, abort"
		    exit 1
		fi
	    fi
	fi
    fi
fi
MAIN_OUT="${SNAPSHOTS}/${VERSION}"

if [ $INCLUDE_NF = 1 ]; then
    NF_RELEASE="/mnt/nfs-cdimage/unofficial/non-free/images-including-firmware/${VERSION}+nonfree"
    if [ ! -d "$NF_RELEASE" ]; then
	NF_RELEASE="/mnt/nfs-cdimage/unofficial/non-free/images-including-firmware/archive/${VERSION}+nonfree"
	if [ ! -d "$NF_RELEASE" ]; then
	    NF_RELEASE="/mnt/nfs-cdimage/unofficial/non-free/images-including-firmware/.${VERSION}/debian-cd"
	    if [ ! -d "$NF_RELEASE" ]; then
		echo "Can't find $NF_RELEASE, abort"
		exit 1
	    fi
	fi
    fi
    NF_OUT="${SNAPSHOTS}/${VERSION}+nonfree"
fi

# Check that we don't already have files in the snap dir
if [ -d "$MAIN_OUT" ]; then
    echo "$MAIN_OUT already exists. abort"
    exit 1
fi

# Check that we don't already have files in the snap dir
if [ -d "$NF_OUT" ]; then
    echo "$NF_OUT already exists. abort"
    exit 1
fi

echo "Copying files"
mkdir "$MAIN_OUT"
cd "$MAIN_RELEASE"
find . -name '*.jigdo' | xargs tar cf - | (cd $MAIN_OUT && tar xvf -)
if [ $INCLUDE_NF = 1 ]; then
    mkdir "$NF_OUT"
    cd "$NF_RELEASE"
    find . -name '*.jigdo' | xargs tar cf - | (cd $NF_OUT && tar xvf -)
fi

echo "Making snapshot"
~/bin/weekly-snapshots nocopy
