export PUBDIRJIG=$PUBDIR/daily-builds
if [ "$BUILDNUM"x = ""x ]; then
    BUILDNUM=1
fi
export DATE_BUILD="$DATE-$BUILDNUM"
export CD_I_F="${OUT_BASE}/unofficial/non-free/cd-including-firmware"
export TESTING_SUITE=${CODENAME}
export NI_WANTED=1
export TESTING_WANTED=0
